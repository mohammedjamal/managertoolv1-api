package com.hu.api.managertoolbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManagertoolBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(ManagertoolBackendApplication.class, args);
	}

}
